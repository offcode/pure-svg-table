# Pure SVG table

SVG has [no table-support](https://stackoverflow.com/questions/6987005/create-a-table-in-svg).
There are a couple of options to workaround this.

- You can use custom elements with a table-like semantics. 
This is the approach used by the [GraphTableSVG](https://www.npmjs.com/package/graph-table-svg) module 
which needs some additional javascript to be executed.
- Another approach is to [workaround it](http://svg-whiz.com/svg/table.svg) using `text` and `tspan` elements.
- Yet another approach is to embed the html table inside a `foreignObject` element 
and adding the html styles under the `style` element.  The current module takes this approach.
