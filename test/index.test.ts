process.env.TEST = "mocha"

const assert = require('assert')
const fs = require('fs')
const svgTable = require('../src/index')

const data = [
    ["A1", "A2"],
    ["B1", "B2"],
]

it("Tag data", () => {
    const expected = {
        table: {
            tr: [
                {td: ["A1", "A2"]},
                {td: ["B1", "B2"]},
            ]
        }
    }
    const tagged = svgTable.tagData(data)
    assert.deepStrictEqual(tagged, expected)
})

it("Complex cell", () => {
    const data = [
        [{img: {
            "@src": "http://example.com/cat.jpg"
            }}],
    ]
    const got = svgTable.generateTable(data)
    assert.match(got, new RegExp('<td>\\s*<img src="http://example.com/cat.jpg" />\\s*</td>'))
})

describe("Generate SVG", () => {
    it("has no style", () => {
        const dataPath = "./test/data.json"
        const resultPath = "./tmp.svg"
        const expectedPath = "./test/plain.svg"
        svgTable.generateTableFromFile(dataPath, resultPath)
        const result = fs.readFileSync(resultPath).toString()
        const expected = fs.readFileSync(expectedPath).toString()
        assert.strictEqual(result, expected)
    })
    it("with style", () => {
        const dataPath = "./test/data.json"
        const cssPath = "./test/style.css"
        const resultPath = "./tmp.svg"
        const expectedPath = "./test/styled.svg"
        svgTable.generateTableFromFile(dataPath, resultPath, cssPath)
        const result = fs.readFileSync(resultPath).toString()
        const expected = fs.readFileSync(expectedPath).toString()
        assert.strictEqual(result, expected)
    })
    it("from the command line", () => {
        const args = [
            "--data", "./test/data.json",
            "--css", "./test/style.css",
            "--output", "./tmp.svg",
        ]
        svgTable.program.parse(args, {from: "user"})
    })
})
