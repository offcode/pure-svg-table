import { create } from 'xmlbuilder2';
import { program } from 'commander';
import * as fs from 'fs';

function tagData(data: any[]) {
  const rows = data.map((row) => {
    return { td: row };
  });
  return {
    table: {
      tr: rows
    }
  };
}

export function generateTable(data: any[][], style?: string): string {
  const taggedData = {
    svg: {
      '@xmlns': 'http://www.w3.org/2000/svg',
      style,
      foreignObject: {
        '@x': 0,
        '@y': 0,
        '@width': '100%',
        '@height': '100%',
        body: {
          '@xmlns': 'http://www.w3.org/1999/xhtml',
          table: tagData(data).table
        }
      }
    }
  };
  return create(taggedData).end({ prettyPrint: true });
}

export function generateTableFromFile(dataPath: string, outputPath: string, cssPath?: string) {
  const data = JSON.parse(fs.readFileSync(dataPath).toString());
  let style
  if (cssPath !== undefined) {
    style = fs.readFileSync(cssPath).toString();
  }
  const xml = generateTable(data, style)
  fs.writeFileSync(outputPath, xml);
}

program
  .requiredOption('-d, --data <path>', 'Path to the json file that holds the data')
  .requiredOption('-o, --output <result.svg>', 'Target file')
  .option('-c, --css <css>', 'Path to css file')
  .action((cmd) => {
    generateTableFromFile(cmd.data, cmd.output, cmd.css);
  });

if (require.main === module) {
  program.parse(process.argv);
}

if (process.env.TEST) {
  module.exports = { tagData, program, generateTable, generateTableFromFile }
}
else {
  module.exports = { generateTable, generateTableFromFile }
}
